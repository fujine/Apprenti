#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "constantes.h"

void hud(SDL_Surface *ecran, int vie, int feu, int glace, int terre, int vent, int latence_feu, int latence_glace, int latence_terre, int latence_vent, int niveau)
{
    SDL_Surface *barredevie=NULL, *barredesorts=NULL;
    SDL_Surface *coeurpng=NULL, *feupng=NULL, *glacepng=NULL, *terrepng=NULL, *ventpng=NULL, *niveaupng=NULL;
    SDL_Rect poscoeurpng, posfeupng, posglacepng, posterrepng, posventpng, posniveaupng, posbarredesorts;

    barredevie=IMG_Load("Images/hud/barredevie.png");
    barredesorts= IMG_Load("Images/hud/barredesort.png");

    //Position des éléments
    poscoeurpng.x=0;
    poscoeurpng.y=0;
    posfeupng.x=0;
    posfeupng.y=594;
    posglacepng.x=0;
    posglacepng.y=528;
    posterrepng.x=0;
    posterrepng.y=462;
    posventpng.x=0;
    posventpng.y=396;
    posbarredesorts.x=0;
    posbarredesorts.y=386;
    posniveaupng.x=1254;
    posniveaupng.y=0;

    //Gestion du niveau
    switch (niveau)
    {
        default:
            break;
        case 1:
            niveaupng = IMG_Load("Images/hud/niveau1.png");
            break;
        case 2:
            niveaupng = IMG_Load("Images/hud/niveau2.png");
            break;
        case 3:
            niveaupng = IMG_Load("Images/hud/niveau3.png");
            break;
        case 4:
            niveaupng = IMG_Load("Images/hud/niveau4.png");
            break;
        case 5:
            niveaupng = IMG_Load("Images/hud/niveau5.png");
            break;
        case 6:
            niveaupng = IMG_Load("Images/hud/niveau6.png");
            break;
        case 7:
            niveaupng = IMG_Load("Images/hud/niveau7.png");
            break;
        case 8:
            niveaupng = IMG_Load("Images/hud/niveau8.png");
            break;
        case 9:
            niveaupng = IMG_Load("Images/hud/niveau9.png");
            break;
        case 10:
            niveaupng = IMG_Load("Images/hud/niveau10.png");
            break;
    }
    //Gestion de la barre de vie
    switch (vie)
    {
        default:
            break;
        case 1:
            coeurpng = IMG_Load("Images/hud/coeur1.png");
            break;
        case 2:
            coeurpng = IMG_Load("Images/hud/coeur2.png");
            break;
        case 3:
            coeurpng = IMG_Load("Images/hud/coeur3.png");
            break;
        case 4:
            coeurpng = IMG_Load("Images/hud/coeur4.png");
            break;
        case 5:
            coeurpng = IMG_Load("Images/hud/coeur5.png");
            break;
        case 6:
            coeurpng = IMG_Load("Images/hud/coeur6.png");
            break;
        case 7:
            coeurpng = IMG_Load("Images/hud/coeur7.png");
            break;
        case 8:
            coeurpng = IMG_Load("Images/hud/coeur8.png");
            break;
        case 9:
            coeurpng = IMG_Load("Images/hud/coeur9.png");
            break;
        case 10:
            coeurpng = IMG_Load("Images/hud/coeur10.png");
            break;
    }
    //Gestion des pouvoirs: 0=non dispo, 1=dispo et sélectionné, 2=dispo non sélectionné
    switch (feu)
    {
        default:
            break;
        case 1:
            if(latence_feu == 0)
                feupng = IMG_Load("Images/hud/symbolefeu1.png");
            else
                feupng = IMG_Load("Images/hud/symbolefeu0.png");
            break;
        case 2:
            if(latence_feu >2)
                feupng = IMG_Load("Images/hud/symbolefeu2.png");
            else if (latence_feu > 0)
                feupng = IMG_Load("Images/hud/symbolefeu0.png");
            else
                feupng = IMG_Load("Images/hud/symbolefeu2.png");
            break;
    }
    switch (glace)
    {
        default:
            break;
        case 1:
            if(latence_glace == 0)
                glacepng = IMG_Load("Images/hud/symboleglace1.png");
            else
                glacepng = IMG_Load("Images/hud/symboleglace0.png");
            break;
        case 2:
            if(latence_glace > 37)
                glacepng = IMG_Load("Images/hud/symboleglace2.png");
            else if(latence_glace > 0)
                glacepng = IMG_Load("Images/hud/symboleglace0.png");
            else
                glacepng = IMG_Load("Images/hud/symboleglace2.png");
            break;
    }
    switch (terre)
    {
        default:
            break;
        case 1:
            if(latence_terre == 0)
                terrepng = IMG_Load("Images/hud/symboleterre1.png");
            else
                terrepng = IMG_Load("Images/hud/symboleterre0.png");
            break;
        case 2:
            if(latence_terre > 12)
                terrepng = IMG_Load("Images/hud/symboleterre2.png");
            else if(latence_terre > 0)
                terrepng = IMG_Load("Images/hud/symboleterre0.png");
            else
                terrepng = IMG_Load("Images/hud/symboleterre2.png");
            break;
    }
    switch (vent)
    {
        default:
            break;
        case 1:
            if(latence_vent == 0)
                ventpng = IMG_Load("Images/hud/symbolevent1.png");
            else
                ventpng = IMG_Load("Images/hud/symbolevent0.png");
            break;
        case 2:
            if(latence_vent > 22)
                ventpng = IMG_Load("Images/hud/symbolevent2.png");
            else if (latence_vent >0)
                ventpng = IMG_Load("Images/hud/symbolevent0.png");
            else
                ventpng = IMG_Load("Images/hud/symbolevent2.png");
            break;
    }
    //Blit des éléments
    SDL_BlitSurface(niveaupng, NULL, ecran, &posniveaupng);
    SDL_BlitSurface(barredevie, NULL, ecran, &poscoeurpng);
    poscoeurpng.x=5;

    SDL_BlitSurface(barredesorts, NULL, ecran, &posbarredesorts);
    SDL_BlitSurface(feupng, NULL, ecran, &posfeupng);
    SDL_BlitSurface(terrepng, NULL, ecran, &posterrepng);
    SDL_BlitSurface(glacepng, NULL, ecran, &posglacepng);
    SDL_BlitSurface(ventpng, NULL, ecran, &posventpng);
    SDL_BlitSurface(coeurpng, NULL, ecran, &poscoeurpng);
    SDL_FreeSurface(niveaupng);
    SDL_FreeSurface(barredesorts);
    SDL_FreeSurface(feupng);
    SDL_FreeSurface(terrepng);
    SDL_FreeSurface(glacepng);
    SDL_FreeSurface(ventpng);
    SDL_FreeSurface(coeurpng);
    SDL_FreeSurface(barredevie);
}

void perdu(SDL_Surface *ecran, int *vie, SDL_Surface *Game_over, SDL_Rect posFond, int *niveau, int *continuer)
{
    SDL_Event event;

    *continuer = 0;
    SDL_BlitSurface(Game_over, NULL, ecran, &posFond);
    SDL_Flip(ecran);
    while (SDL_WaitEvent(&event) && SDL_KEYDOWN!=event.type);
        switch(event.key.keysym.sym)
        {
            default:
                break;
        }
    *niveau = *niveau - 1;
    *vie = 10;
}

void findepartie(SDL_Surface *ecran, int *continuer, SDL_Surface *fin, SDL_Rect posFond)
{
    SDL_Event event;

    *continuer = 0;
    SDL_BlitSurface(fin, NULL, ecran, &posFond);
    SDL_Flip(ecran);
    while (SDL_WaitEvent(&event) && SDL_KEYDOWN!=event.type);
        switch(event.key.keysym.sym)
        {
            default:
                break;
        }
}
