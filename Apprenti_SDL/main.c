/******************************************
*                Jeu L'Apprenti Sorcier                *
*    Created By SAUNIER Guillaume             *
*                        RAGONNEAU Valentin         *
******************************************/

#include"SDL/SDL.h"
#include"constantes.h"
#include"menu.h"

int main ( int argc, char** argv )
{
    SDL_Surface *ecran = NULL;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER);

    //initialisation de la fen�tre via SDL:
    SDL_WM_SetCaption("- L'Apprenti Magicien - ", NULL);
    ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 32, SDL_HWSURFACE);
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 198, 255, 170));
    freopen("CON", "w", stdout);
    freopen("CON", "w", stderr);

    menu(ecran);

    SDL_Quit();

    return EXIT_SUCCESS;
}

