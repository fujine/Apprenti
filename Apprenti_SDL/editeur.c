#include"editeur.h"

//Fonction editeur
void editeur(SDL_Surface *ecran)
{
        // Définition SDL basique
        SDL_Surface *hero[4] = {NULL};
        SDL_Surface *squelette[4] = {NULL};
        SDL_Surface *arbre = NULL, *soldur = NULL, *chemin = NULL, *herbe = NULL, *mur = NULL, *porte = NULL, *heroactuel = NULL, *SqueletteActuel = NULL, *orbe = NULL, *info = NULL, *commande = NULL, *affichage = NULL;
        SDL_Surface *eau = NULL, *autel = NULL, *maisonbg = NULL, *maisonhg = NULL, * maisonhd = NULL, *maisonbd = NULL, *trou = NULL, *vase = NULL, *fontainep = NULL, *fondnoir = NULL, *murbas = NULL;
        SDL_Event event;

        SDL_Rect position, posFond;

        int continuer = 1, i = 0, j = 0, numero = 0, clicGaucheEnCours = 0, clicDroitEnCours = 0;;
        int objetActuel = MUR;
        int carte[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};
        int collision[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};
        int tabvase[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};

        // Chargement des sprites (décors, personnage...)
        commande = IMG_Load("Images/touches.png");
        info = IMG_Load("Images/info.png");
        orbe = IMG_Load("Images/orbe.png");
        mur = IMG_Load("Images/mur.png");
        herbe = IMG_Load("Images/herbe.png");
        soldur = IMG_Load ("Images/soldur.png");
        chemin = IMG_Load ("Images/chemin.png");
        arbre = IMG_Load ("Images/arbre.png");
        porte = IMG_Load("Images/porte.png");
        hero[BAS] = IMG_Load("Images/herobas.png");
        squelette[BAS] = IMG_Load("Images/squelettebas.png");
        murbas = IMG_Load("Images/murbas.png");
        maisonbg = IMG_Load("Images/maisonbg.png");
        maisonhg = IMG_Load("Images/maisonhg.png");
        maisonbd = IMG_Load("Images/maisonbd.png");
        maisonhd = IMG_Load("Images/maisonhd.png");
        fontainep = IMG_Load("Images/fontainep.png");
        eau = IMG_Load("Images/eau.png");
        fondnoir = IMG_Load("Images/fondnoir.png");
        trou = IMG_Load("Images/trou.png");
        vase = IMG_Load("Images/vase.png");
        autel = IMG_Load("Images/autel.png");
        affichage = info;

        posFond.x = 0;
        posFond.y = 0;

        //Position héros départ
        heroactuel = hero[BAS];
        //Position squelette départ
        SqueletteActuel = squelette[BAS];

        int jouer = 1;
        while (jouer)
        {

            numero++;
            if (!chargerNiveau(carte, collision, tabvase, numero))
                exit(EXIT_FAILURE);

            continuer = 1;

            while (continuer)
                {
                    SDL_WaitEvent(&event);
                    switch(event.type)
                    {
                        case SDL_QUIT:
                            continuer = 0;
                            jouer = 0;
                            break;
                        case SDL_MOUSEBUTTONDOWN:
                            if (event.button.button == SDL_BUTTON_LEFT)
                            {
                                // On met l'objet actuellement choisi (mur, caisse...) à l'endroit du clic
                                carte[event.button.x / TAILLE_BLOC][event.button.y / TAILLE_BLOC] = objetActuel;
                                clicGaucheEnCours = 1; // On active un booléen pour retenir qu'un bouton est enfoncé
                            }
                            else if (event.button.button == SDL_BUTTON_RIGHT) // Le clic droit sert à effacer
                            {
                                carte[event.button.x / TAILLE_BLOC][event.button.y /TAILLE_BLOC] = VIDE;
                                clicDroitEnCours = 1;
                            }
                            break;
                        case SDL_MOUSEBUTTONUP: // On désactive le booléen qui disait qu'un bouton était enfoncé
                            if (event.button.button == SDL_BUTTON_LEFT)
                                clicGaucheEnCours = 0;
                            else if (event.button.button == SDL_BUTTON_RIGHT)
                                clicDroitEnCours = 0;
                            break;
                        case SDL_MOUSEMOTION:
                            if (clicGaucheEnCours) // Si on déplace la souris et que le bouton gauche de la souris est enfoncé
                            {
                                carte[event.motion.x / TAILLE_BLOC][event.motion.y / TAILLE_BLOC] = objetActuel;
                            }
                            else if (clicDroitEnCours) // Pareil pour le bouton droit de la souris
                            {
                                carte[event.motion.x / TAILLE_BLOC][event.motion.y / TAILLE_BLOC] = VIDE;
                            }
                            break;
                        case SDL_KEYDOWN:
                            switch(event.key.keysym.sym)
                            {
                                case SDLK_TAB:
                                    continuer = 0;
                                    if(numero == 10)
                                        jouer = 0;
                                    break;
                                case SDLK_s:
                                    sauvegarderNiveau(carte, numero);
                                    break;
                                case SDLK_c:
                                    chargerNiveau(carte, collision, tabvase, numero);
                                    break;
                                case SDLK_a:
                                    objetActuel = MUR;
                                    break;
                                case SDLK_z:
                                    objetActuel = HERBE;
                                    break;
                                case SDLK_e:
                                    objetActuel = SOLDUR;
                                    break;
                                case SDLK_r:
                                    objetActuel = CHEMIN;
                                    break;
                                case SDLK_t:
                                    objetActuel = ARBRE;
                                    break;
                                case SDLK_y:
                                    objetActuel = PORTE;
                                    break;
                                case SDLK_u:
                                    objetActuel = HERO;
                                    break;
                                case SDLK_i:
                                    objetActuel = SQUELETTE;
                                    break;
                                case SDLK_o:
                                    objetActuel = MURBAS;
                                    break;
                                case SDLK_p:
                                    objetActuel = EAU;
                                    break;
                                case SDLK_m:
                                    objetActuel = TROU;
                                    break;
                                case SDLK_l:
                                    objetActuel = VASE;
                                    break;
                                case SDLK_k:
                                    objetActuel = FONTAINEP;
                                    break;
                                case SDLK_j:
                                    objetActuel = FONDNOIR;
                                    break;
                                case SDLK_n:
                                    objetActuel = MAISONBD;
                                    break;
                                case SDLK_b:
                                    objetActuel = MAISONHD;
                                    break;
                                case SDLK_v:
                                    objetActuel = MAISONBG;
                                    break;
                                case SDLK_h:
                                    objetActuel = MAISONHG;
                                    break;
                                case SDLK_g:
                                    objetActuel = AUTEL;
                                    break;
                                case SDLK_x:
                                    objetActuel = GEL;
                                    break;
                                case SDLK_w:
                                    objetActuel = ORBE;
                                    break;
                                case SDLK_SPACE:
                                    if(affichage == info)
                                        affichage = commande;
                                    else
                                        affichage = info;
                                    break;
                                case SDLK_ESCAPE:
                                    continuer = 0;
                                    jouer = 0;
                                    break;
                                default:
                                    break;
                            }
                            break;
                    }

                    // Effacement de l'écran
                    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

                    // Placement des objets à l'écran
                    for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
                    {
                        for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
                        {
                            position.x = i * TAILLE_BLOC;
                            position.y = j * TAILLE_BLOC;

                            switch(carte[i][j])
                            {
                                case MUR:
                                    SDL_BlitSurface(mur, NULL, ecran, &position);
                                    break;
                                case EAU:
                                    SDL_BlitSurface(eau, NULL, ecran, &position);
                                    break;
                                case HERBE:
                                    SDL_BlitSurface(herbe, NULL, ecran, &position);
                                    break;
                                case SOLDUR:
                                    SDL_BlitSurface(soldur, NULL, ecran, &position);
                                    break;
                                case CHEMIN:
                                    SDL_BlitSurface(chemin, NULL, ecran, &position);
                                    break;
                                case ARBRE:
                                    SDL_BlitSurface(arbre, NULL, ecran, &position);
                                    break;
                                case PORTE:
                                    SDL_BlitSurface(porte, NULL, ecran, &position);
                                    break;
                                case HERO:
                                    SDL_BlitSurface(heroactuel, NULL, ecran, &position);
                                    break;
                                case SQUELETTE:
                                    SDL_BlitSurface(SqueletteActuel, NULL, ecran, &position);
                                    break;
                                case TROU:
                                    SDL_BlitSurface(trou, NULL, ecran, &position);
                                    break;
                                case VASE:
                                    SDL_BlitSurface(vase, NULL, ecran, &position);
                                    break;
                                case FONDNOIR:
                                    SDL_BlitSurface(fondnoir, NULL, ecran, &position);
                                    break;
                                case FONTAINEP:
                                    SDL_BlitSurface(fontainep, NULL, ecran, &position);
                                    break;
                                case MURBAS:
                                    SDL_BlitSurface(murbas, NULL, ecran, &position);
                                    break;
                                case MAISONBG:
                                    SDL_BlitSurface(maisonbg, NULL, ecran, &position);
                                    break;
                                case MAISONHG:
                                    SDL_BlitSurface(maisonhg, NULL, ecran, &position);
                                    break;
                                case MAISONBD:
                                    SDL_BlitSurface(maisonbd, NULL, ecran, &position);
                                    break;
                                case MAISONHD:
                                    SDL_BlitSurface(maisonhd, NULL, ecran, &position);
                                    break;
                                case AUTEL:
                                    SDL_BlitSurface(autel, NULL, ecran, &position);
                                    break;
                                case ORBE:
                                    SDL_BlitSurface(orbe, NULL, ecran, &position);
                                    break;
                            }
                        }
                    }

                    // Mise à jour de l'écran
                    SDL_BlitSurface(affichage, NULL, ecran, &posFond);
                    SDL_Flip(ecran);
                }
        }

        SDL_FreeSurface(mur); SDL_FreeSurface(herbe); SDL_FreeSurface(arbre);
        SDL_FreeSurface(chemin); SDL_FreeSurface(soldur); SDL_FreeSurface(porte);
        SDL_FreeSurface(murbas); SDL_FreeSurface(maisonbg);
        SDL_FreeSurface(maisonhg); SDL_FreeSurface(maisonbd); SDL_FreeSurface(maisonhd);
        SDL_FreeSurface(fontainep); SDL_FreeSurface(eau); SDL_FreeSurface(fondnoir); SDL_FreeSurface(trou);
        SDL_FreeSurface(vase); SDL_FreeSurface(autel); SDL_FreeSurface(squelette[BAS]);

        for (i = 0 ; i < 4 ; i++)
            SDL_FreeSurface(hero[i]);


}
