#include"interaction.h"

void interagir(int carte[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], SDL_Rect pos, SDL_Rect posTab, int direction, int *glace, int *terre, int *vent, int *vie, int niveau)
{
    SDL_Rect positionSuivant;
    switch (direction)
    {
        case 1:
            positionSuivant.y = pos.y + DEPLACEMENT;
            positionSuivant.x = pos.x;
            break;
        case 2:
            positionSuivant.y = pos.y + TAILLE_BLOC;
            positionSuivant.x = pos.x;
            break;
        case 3:
            positionSuivant.x = pos.x - DEPLACEMENT;
            positionSuivant.y = pos.y;
            break;
        case 4:
            positionSuivant.x = pos.x + TAILLE_BLOC;
            positionSuivant.y = pos.y;
            break;
    }
    //On cherche l'objet avec lequel interagir
    if (tabvase[positionSuivant.x / TAILLE_BLOC][positionSuivant.y / TAILLE_BLOC] == 5)
    {
        if (*vie<10)
        {
            *vie=*vie+1;
            tabvase[positionSuivant.x / TAILLE_BLOC][positionSuivant.y / TAILLE_BLOC]=0;
        }
    }
    if(positionSuivant.y != pos.y && pos.x % TAILLE_BLOC != 0) // Quand personnage a cheval sur deux cases
    {
        if (tabvase[(positionSuivant.x / TAILLE_BLOC) + 1][positionSuivant.y / TAILLE_BLOC] == 5 && *vie < 10)
        {
            *vie=*vie+1;
            tabvase[(positionSuivant.x / TAILLE_BLOC) + 1][positionSuivant.y / TAILLE_BLOC]=0;
        }
    }
    if(positionSuivant.x != pos.x && pos.y % TAILLE_BLOC != 0)
    {
        if (tabvase[positionSuivant.x / TAILLE_BLOC][(positionSuivant.y / TAILLE_BLOC) + 1] == 5 && *vie < 10)
        {
            *vie=*vie+1;
            tabvase[positionSuivant.x / TAILLE_BLOC][(positionSuivant.y / TAILLE_BLOC) + 1]=0;
        }
    }
   if (carte[positionSuivant.x/TAILLE_BLOC][positionSuivant.y / TAILLE_BLOC] == AUTEL)
    {
        if (niveau==3 && *glace==0)
            *glace=2;
        if (niveau==5 && *terre==0)
            *terre=2;
        if (niveau==7 && *vent==0)
            *vent=2;
    }
    if(positionSuivant.y != pos.y && pos.x % TAILLE_BLOC != 0)
    {
        if (carte[(positionSuivant.x / TAILLE_BLOC) + 1][positionSuivant.y / TAILLE_BLOC] == AUTEL)
        {
            if (niveau==3 && *glace==0)
                *glace=2;
            if (niveau==5 && *terre==0)
                *terre=2;
            if (niveau==7 && *vent==0)
                *vent=2;
        }
    }
    if(positionSuivant.x != pos.x && pos.y % TAILLE_BLOC != 0)
    {
        if (carte[positionSuivant.x / TAILLE_BLOC][(positionSuivant.y / TAILLE_BLOC) + 1] == AUTEL)
        {
            if (niveau==3 && *glace==0)
                *glace=2;
            if (niveau==5 && *terre==0)
                *terre=2;
            if (niveau==7 && *vent==0)
                *vent=2;
        }
    }
}

void bouleFeu(SDL_Rect *pos, SDL_Rect *pos_bouledefeu, int *boule, int *direction_hero, int *direction_boule)
{

    if(*boule <= PORTEE)
    {
        if(*boule >= 2)
        {
            switch (*direction_boule)
            {
                case 1 : //HAUT
                    pos_bouledefeu->y = pos_bouledefeu->y - DEPLACEMENT;
                    break;
                case 2 : //BAS
                    pos_bouledefeu->y = pos_bouledefeu->y + DEPLACEMENT;
                    break;
                case 3 : //GAUCHE
                    pos_bouledefeu->x = pos_bouledefeu->x - DEPLACEMENT;
                    break;
                case 4 : //DROIT
                    pos_bouledefeu->x = pos_bouledefeu->x + DEPLACEMENT;
                    break;
            }
            *boule = *boule + 1;
        }
        else if(*boule == 1)
        {
            //On regarde vers où regarde le joueur
            switch (*direction_hero)
            {
                case 1 : //HAUT
                    pos_bouledefeu->x = pos->x;
                    pos_bouledefeu->y = pos->y - DEPLACEMENT;
                    *direction_boule = 1;
                    break;
                case 2:  //BAS
                    pos_bouledefeu->x = pos->x;
                    pos_bouledefeu->y = pos->y + DEPLACEMENT;
                    *direction_boule = 2;
                    break;
                case 3:  //GAUCHE
                    pos_bouledefeu->x = pos->x - DEPLACEMENT;
                    pos_bouledefeu->y = pos->y;
                    *direction_boule = 3;
                    break;
                case 4:  //DROITE
                    pos_bouledefeu->x = pos->x + DEPLACEMENT;
                    pos_bouledefeu->y = pos->y;
                    *direction_boule = 4;
                    break;
            }
            *boule = 2;
        }
    }

}

int explosion(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect posboule, int tabSque[NBSQUE][CARASQUE], int directionboule, int nb_sque)
{
    int explo = -2, i = 0, toucher = 0;

    //boule de feu explose quand en contacte avec du decor ou des sque:
    switch(directionboule)
    {
        case 1: //HAUT
            if (carte[posboule.x / TAILLE_BLOC][posboule.y / TAILLE_BLOC] == MUR)
                explo = -1;
            else if(carte[(posboule.x +  DEPLACEMENT) / TAILLE_BLOC][posboule.y / TAILLE_BLOC] == MUR)
                explo = -1;
            else if(carte[(posboule.x + 2*DEPLACEMENT) / TAILLE_BLOC][posboule.y / TAILLE_BLOC] == MUR)
                explo = -1;
            if (carte[posboule.x / TAILLE_BLOC][posboule.y / TAILLE_BLOC] == 7)
                explo = -3;
            else if(carte[(posboule.x +  DEPLACEMENT) / TAILLE_BLOC][posboule.y / TAILLE_BLOC] == 7)
                explo = -3;
            else if(carte[(posboule.x + 2*DEPLACEMENT) / TAILLE_BLOC][posboule.y / TAILLE_BLOC] == 7)
                explo = -3;
            break;
        case 2: //BAS
            if (carte[posboule.x / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == MUR)
                explo = -1;
            else if(carte[(posboule.x + DEPLACEMENT) / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == MUR)
                explo = -1;
            else if(carte[(posboule.x + 2*DEPLACEMENT) / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT)/ TAILLE_BLOC] == MUR)
                explo = -1;
             if (carte[posboule.x / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == 7)
                explo = -3;
            else if(carte[(posboule.x + DEPLACEMENT) / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == 7)
                explo = -3;
            else if(carte[(posboule.x + 2*DEPLACEMENT) / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT)/ TAILLE_BLOC] == 7)
                explo = -3;
            break;
        case 3: //GAUCHE
            if (carte[posboule.x / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == MUR)
                explo = -1;
            if (carte[posboule.x / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == 7 )
                explo = -3;
            break;
        case 4: // DROITE
            if (carte[(posboule.x + 2 * DEPLACEMENT) / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == MUR)
                explo = -1;
            if (carte[(posboule.x + 2 * DEPLACEMENT) / TAILLE_BLOC][(posboule.y + 2 * DEPLACEMENT) / TAILLE_BLOC] == 7)
                explo = -1;
            break;
    }

    for (i = 0; i < nb_sque; i++)
    {
        if (posboule.x == tabSque[i][POSX] && posboule.y == tabSque[i][POSY])
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] && posboule.y == tabSque[i][POSY] + DEPLACEMENT)
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] && posboule.y == tabSque[i][POSY] + 2 *DEPLACEMENT)
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] + DEPLACEMENT && posboule.y == tabSque[i][POSY])
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] + DEPLACEMENT && posboule.y == tabSque[i][POSY] + DEPLACEMENT)
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] + DEPLACEMENT && posboule.y == tabSque[i][POSY] + 2 * DEPLACEMENT)
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] + 2 * DEPLACEMENT && posboule.y == tabSque[i][POSY])
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] + 2 * DEPLACEMENT && posboule.y == tabSque[i][POSY] + DEPLACEMENT)
            toucher = 1;
        else if (posboule.x == tabSque[i][POSX] + 2 * DEPLACEMENT && posboule.y == tabSque[i][POSY] + 2 * DEPLACEMENT)
            toucher = 1;
        if(toucher == 1)
        {
            tabSque[i][10] = tabSque[i][10] - 1;
            explo = i;
            nb_sque = 0;
        }
        toucher = 0;
    }
    return explo;
}

 void glacier(int carte[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, int tabsque[NBSQUE][CARASQUE], SDL_Rect *posGlace, int direction_hero, int nb_sque, int *direction_glace)
{
    int i = 0, j = 0;
    SDL_Rect portee_min, portee_max;

    //Calcule de la zone d'effet et trouve si de l'eau est dans la zone et la gel:
    switch(direction_hero)
    {
        case 1 :
            portee_max.y = pos->y - 6 * DEPLACEMENT;
            portee_min.y = pos->y - 11 * DEPLACEMENT;
            portee_min.x = pos->x - 5 * DEPLACEMENT;
            portee_max.x = pos->x + 5 * DEPLACEMENT;
            posGlace->x = pos->x - 3 * DEPLACEMENT;
            posGlace->y = pos->y - 9 * DEPLACEMENT;
            *direction_glace = 1;
            for(j = 1; j < 3; j ++)
            {
                for(i = 1 ; i < 4; i++)
                {
                    if(carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] == 16)
                    {
                        carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 20;
                        collision[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 0;
                    }
                }
            }
            break;
        case 2 :
            portee_min.y = pos->y  + 4 * DEPLACEMENT;
            portee_max.y = pos->y  + 9 * DEPLACEMENT;
            portee_min.x = pos->x - 5 * DEPLACEMENT;
            portee_max.x = pos->x + 5 * DEPLACEMENT;
            posGlace->x = pos->x - 3 * DEPLACEMENT;
            posGlace->y = pos->y + 6 * DEPLACEMENT;
            *direction_glace = 1;
            for(j = 1; j < 3; j ++)
            {
                for(i = 1 ; i < 4; i++)
                {
                    if(carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] == 16)
                    {
                        carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 20;
                        collision[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 0;
                    }
                }
            }
            break;
        case 3 :
            portee_max.y = pos->y  + 5 * DEPLACEMENT;
            portee_min.y = pos->y  - 5 * DEPLACEMENT;
            portee_max.x = pos->x - 4 * DEPLACEMENT;
            portee_min.x = pos->x - 11 * DEPLACEMENT;
            posGlace->x = pos->x - 9 * DEPLACEMENT;
            posGlace->y = pos->y - 3 * DEPLACEMENT;
            *direction_glace = 0;
            for(j = 1; j < 4; j ++)
            {
                for(i = 1 ; i < 3; i++)
                {
                    if(carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] == 16)
                    {
                        carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 20;
                        collision[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 0;
                    }
                }
            }
            break;
        case 4 :
            portee_max.y = pos->y  + 5 * DEPLACEMENT;
            portee_min.y = pos->y  - 5 * DEPLACEMENT;
            portee_min.x = pos->x + 4 * DEPLACEMENT;
            portee_max.x = pos->x + 11 * DEPLACEMENT;
            posGlace->x = pos->x + 6 * DEPLACEMENT;
            posGlace->y = pos->y - 3 * DEPLACEMENT;
            *direction_glace = 0;
            for(j = 1; j < 4; j ++)
            {
                for(i = 1 ; i < 3; i++)
                {
                    if(carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] == 16)
                    {
                        carte[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 20;
                        collision[(portee_min.x / TAILLE_BLOC) + i][(portee_min.y / TAILLE_BLOC) + j] = 0;
                    }
                }
            }
            break;
    }

    for( i = 0 ; i < nb_sque ; i ++)
    {
        if (tabsque[i][POSX] >= portee_min.x && tabsque[i][POSX] <= portee_max.x )
        {
            if (tabsque[i][POSY] >= portee_min.y && tabsque[i][POSY] <= portee_max.y)
            {
                tabsque[i][GLACER] = 20;
            }
        }
    }


}

int creationMur(SDL_Rect posSuiv, SDL_Rect pos, int tabSque[NBSQUE][CARASQUE], int direction_terre, int tabTerre[3][3], int nb_sque)
{
    int j = 0, bloquer = 0;

    //Vérifie la place pour créer un mur de Terre:
    switch(direction_terre)
    {
        case 0:
            for (j = 0 ; j < nb_sque ; j ++)
            {
                if(bloquer == 0)
                {
                    if(pos.x == tabSque[j][POSX])
                    {
                        if(posSuiv.y == tabSque[j][POSY] || posSuiv.y - DEPLACEMENT == tabSque[j][POSY] || posSuiv.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                        }
                    else if(pos.x - DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(posSuiv.y == tabSque[j][POSY] || posSuiv.y - DEPLACEMENT == tabSque[j][POSY] || posSuiv.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                    else if(pos.x - 2 * DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(posSuiv.y == tabSque[j][POSY] || posSuiv.y - DEPLACEMENT == tabSque[j][POSY] || posSuiv.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                    else if(pos.x + DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(posSuiv.y == tabSque[j][POSY] || posSuiv.y - DEPLACEMENT == tabSque[j][POSY] || posSuiv.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                    else if(pos.x + 2 * DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(posSuiv.y == tabSque[j][POSY] || posSuiv.y - DEPLACEMENT == tabSque[j][POSY] || posSuiv.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + DEPLACEMENT == tabSque[j][POSY] || posSuiv.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                }
                else
                    break;
            }
            for (j = 0; j < 3; j++)
            {
                if(bloquer == 0)
                {
                    if(pos.x == tabTerre[j][X])
                    {
                        if(posSuiv.y == tabTerre[j][Y] || posSuiv.y - DEPLACEMENT == tabTerre[j][Y] || posSuiv.y - 2 * DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(pos.x - DEPLACEMENT == tabTerre[j][X])
                    {
                        if(posSuiv.y == tabTerre[j][Y] || posSuiv.y - DEPLACEMENT == tabTerre[j][Y] || posSuiv.y - 2 * DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(pos.x - 2 * DEPLACEMENT == tabTerre[j][X])
                    {
                        if(posSuiv.y == tabTerre[j][Y] || posSuiv.y - DEPLACEMENT == tabTerre[j][Y] || posSuiv.y - 2 * DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(pos.x + DEPLACEMENT == tabTerre[j][X])
                    {
                        if(posSuiv.y == tabTerre[j][Y] || posSuiv.y - DEPLACEMENT == tabTerre[j][Y] || posSuiv.y - 2 * DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(pos.x + 2 * DEPLACEMENT == tabTerre[j][X])
                    {
                        if(posSuiv.y == tabTerre[j][Y] || posSuiv.y - DEPLACEMENT == tabTerre[j][Y] || posSuiv.y - 2 * DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + DEPLACEMENT == tabTerre[j][Y] || posSuiv.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                }
                else
                    break;
            }
        case 1:
            for (j = 0 ; j < nb_sque ; j ++)
            {
                if(bloquer == 0)
                {
                    if(posSuiv.x == tabSque[j][POSX])
                    {
                        if(pos.y == tabSque[j][POSY] || pos.y - DEPLACEMENT == tabSque[j][POSY] || pos.y - 2 * DEPLACEMENT == tabSque[j][POSY] || pos.y + DEPLACEMENT == tabSque[j][POSY] || pos.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                    else if(posSuiv.x - DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(pos.y == tabSque[j][POSY] || pos.y - DEPLACEMENT == tabSque[j][POSY] || pos.y - 2 * DEPLACEMENT == tabSque[j][POSY] || pos.y + DEPLACEMENT == tabSque[j][POSY] || pos.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                    else if(posSuiv.x - 2 * DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(pos.y == tabSque[j][POSY] || pos.y - DEPLACEMENT == tabSque[j][POSY] || pos.y - 2 * DEPLACEMENT == tabSque[j][POSY] || pos.y + DEPLACEMENT == tabSque[j][POSY] || pos.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                    else if(posSuiv.x + DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(pos.y == tabSque[j][POSY] || pos.y - DEPLACEMENT == tabSque[j][POSY] || pos.y - 2 * DEPLACEMENT == tabSque[j][POSY] || pos.y + DEPLACEMENT == tabSque[j][POSY] || pos.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                    else if(posSuiv.x + 2 * DEPLACEMENT == tabSque[j][POSX])
                    {
                        if(pos.y == tabSque[j][POSY] || pos.y - DEPLACEMENT == tabSque[j][POSY] || pos.y - 2 * DEPLACEMENT == tabSque[j][POSY] || pos.y + DEPLACEMENT == tabSque[j][POSY] || pos.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                            bloquer = 1;
                    }
                }
                else
                    break;
            }
            for (j = 0 ; j < 3; j ++)
            {
                if(bloquer == 0)
                {
                    if(posSuiv.x == tabTerre[j][X])
                    {
                        if(pos.y == tabTerre[j][Y] || pos.y - DEPLACEMENT == tabTerre[j][Y] || pos.y - 2 * DEPLACEMENT == tabTerre[j][Y] || pos.y + DEPLACEMENT == tabTerre[j][Y] || pos.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(posSuiv.x - DEPLACEMENT == tabTerre[j][X])
                    {
                        if(pos.y == tabTerre[j][Y] || pos.y - DEPLACEMENT == tabTerre[j][Y] || pos.y - 2 * DEPLACEMENT == tabTerre[j][Y] || pos.y + DEPLACEMENT == tabTerre[j][Y] || pos.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(posSuiv.x - 2 * DEPLACEMENT == tabTerre[j][X])
                    {
                        if(pos.y == tabTerre[j][Y] || pos.y - DEPLACEMENT == tabTerre[j][Y] || pos.y - 2 * DEPLACEMENT == tabTerre[j][Y] || pos.y + DEPLACEMENT == tabTerre[j][Y] || pos.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(posSuiv.x + DEPLACEMENT == tabTerre[j][X])
                    {
                        if(pos.y == tabTerre[j][Y] || pos.y - DEPLACEMENT == tabTerre[j][Y] || pos.y - 2 * DEPLACEMENT == tabTerre[j][Y] || pos.y + DEPLACEMENT == tabTerre[j][Y] || pos.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                    else if(posSuiv.x + 2 * DEPLACEMENT == tabTerre[j][X])
                    {
                        if(pos.y == tabTerre[j][Y] || pos.y - DEPLACEMENT == tabTerre[j][Y] || pos.y - 2 * DEPLACEMENT == tabTerre[j][Y] || pos.y + DEPLACEMENT == tabTerre[j][Y] || pos.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                            bloquer = 1;
                    }
                }
                else
                    break;
            }
    }
    return bloquer;
}

void murTerre(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect pos, SDL_Rect posTab, int *blocTerre, int tabTerre[3][3], int direction_hero, int tabSque[NBSQUE][CARASQUE], int nb_sque)
{
    SDL_Rect positionSuivant;
    int bloquer = 0;
    switch (direction_hero - 1)
    {
        case HAUT:
            positionSuivant.y = pos.y - DEPLACEMENT;
            break;
        case BAS:
            positionSuivant.y = pos.y + TAILLE_BLOC;
            break;
        case GAUCHE:
            positionSuivant.x = pos.x - TAILLE_BLOC;
            break;
        case DROITE:
            positionSuivant.x = pos.x + TAILLE_BLOC;
            break;
    }
    switch (direction_hero - 1)
    {
        case HAUT:
            if (positionSuivant.y < 0)
                break;
            if (carte[pos.x / TAILLE_BLOC][positionSuivant.y / TAILLE_BLOC] == MUR)
                break;
            if (pos.x % TAILLE_BLOC != 0)
            {
                if (carte[posTab.x + 1][positionSuivant.y / TAILLE_BLOC] == MUR)
                    break;
            }
            bloquer = creationMur(positionSuivant, pos, tabSque, 0, tabTerre, nb_sque);
            if(bloquer == 0)
            {
                //génère le bloc de Terre
                tabTerre[*blocTerre][X] = pos.x;
                tabTerre[*blocTerre][Y] = positionSuivant.y;
                tabTerre[*blocTerre][ETAT] = 100; //compteur avant disparition
                if(*blocTerre != 2)
                    *blocTerre = *blocTerre + 1;
                else
                    *blocTerre = 0;
            }
            break;
        case BAS:
            if (positionSuivant.y / TAILLE_BLOC >= DEPLACEMENT * NB_BLOCS_HAUTEUR - DEPLACEMENT - 1)
                break;
            if (carte[pos.x / TAILLE_BLOC][positionSuivant.y / TAILLE_BLOC] == MUR)
                break;
            if (pos.y % TAILLE_BLOC != 0)
            {
                if(carte[pos.x / TAILLE_BLOC][(positionSuivant.y + TAILLE_BLOC)/TAILLE_BLOC] == MUR)
                    break;
                if(carte[(pos.x + TAILLE_BLOC) / TAILLE_BLOC][(positionSuivant.y + TAILLE_BLOC)/TAILLE_BLOC] == MUR)
                    break;
            }
            if (pos.x % TAILLE_BLOC != 0)
            {
                if (carte[(pos.x + TAILLE_BLOC) / TAILLE_BLOC][positionSuivant.y / TAILLE_BLOC] == MUR)
                    break;
            }
            bloquer = creationMur(positionSuivant, pos, tabSque, 0, tabTerre, nb_sque);
            if(bloquer == 0)
            {
                tabTerre[*blocTerre][X] = pos.x;
                tabTerre[*blocTerre][Y] = positionSuivant.y;
                tabTerre[*blocTerre][ETAT] = 100;
                if(*blocTerre != 2)
                    *blocTerre = *blocTerre + 1;
                else
                    *blocTerre = 0;
            }
            break;
        case GAUCHE:
            if (positionSuivant.x  < 0)
                break;
            if (carte[positionSuivant.x / TAILLE_BLOC][posTab.y] == MUR)
                break;
            if (pos.y % TAILLE_BLOC != 0)
            {
                if (carte[positionSuivant.x / TAILLE_BLOC][posTab.y - 1] == MUR)
                    break;
            }
            bloquer = creationMur(positionSuivant, pos, tabSque, 1,tabTerre, nb_sque);
            if(bloquer == 0)
            {
                tabTerre[*blocTerre][X] = positionSuivant.x;
                tabTerre[*blocTerre][Y] = pos.y;
                tabTerre[*blocTerre][ETAT] = 100;
                if(*blocTerre != 2)
                    *blocTerre = *blocTerre + 1;
                else
                    *blocTerre = 0;
            }
            break;
        case DROITE:
            if (positionSuivant.x / TAILLE_BLOC >=  NB_BLOCS_LARGEUR)
                break;
            if (carte[positionSuivant.x / TAILLE_BLOC][posTab.y] == MUR)
                break;
            if (pos.x % TAILLE_BLOC != 0)
            {
                if(carte[(positionSuivant.x + TAILLE_BLOC)/TAILLE_BLOC][posTab.y] == MUR)
                    break;
            }
            if (pos.y % TAILLE_BLOC != 0)
            {
                if (carte[positionSuivant.x / TAILLE_BLOC][posTab.y - 1] == MUR)
                    break;
                if (carte[(positionSuivant.x + TAILLE_BLOC) / TAILLE_BLOC][posTab.y - 1] == MUR)
                    break;
            }
            bloquer = creationMur(positionSuivant, pos, tabSque, 1, tabTerre, nb_sque);
            if(bloquer == 0)
            {
                tabTerre[*blocTerre][X] = positionSuivant.x;
                tabTerre[*blocTerre][Y] = pos.y;
                tabTerre[*blocTerre][ETAT] = 100;
                if(*blocTerre != 2)
                    *blocTerre = *blocTerre + 1;
                else
                    *blocTerre = 0;
            }
            break;
        }
}

void lamedeVent(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, SDL_Rect *posTab, SDL_Rect *posVent, int *direction_vent, int *wind , int direction_hero)
{
    if(*wind == 0)//Quand sort vient d'être lancer:
    {
        //On regarde vers où regarde le joueur
        switch (direction_hero)
        {
            case 1 : //HAUT
                posVent->x = pos->x;
                posVent->y = pos->y - DEPLACEMENT;
                *direction_vent = 1;
                break;
            case 2:  //BAS
                posVent->x = pos->x;
                posVent->y = pos->y + DEPLACEMENT;
                *direction_vent = 2;
                break;
            case 3:  //GAUCHE
                posVent->x = pos->x - DEPLACEMENT;
                posVent->y = pos->y;
                *direction_vent = 3;
                break;
            case 4:  //DROITE
                posVent->x = pos->x + DEPLACEMENT;
                posVent->y = pos->y;
                *direction_vent = 4;
                break;
        }
    }

    //Vérifie les collisions avec le décors:
    if (posVent->x / TAILLE_BLOC >= DEPLACEMENT * NB_BLOCS_LARGEUR - DEPLACEMENT - 1)
        *wind = 0;
    else if (posVent->x < 0)
        *wind = 0;
    else if (posVent->y < 0)
        *wind = 0;
    else if (posVent->y !=  pos->y && pos->x % TAILLE_BLOC != 0 && carte[posTab->x + 1][posVent->y / TAILLE_BLOC] == MUR)
        *wind = 0;
    else if (posVent->y / TAILLE_BLOC >= DEPLACEMENT * NB_BLOCS_HAUTEUR - DEPLACEMENT - 1)
        *wind = 0;
    else if (posVent->x != pos->x && carte[posVent->x / TAILLE_BLOC][posTab->y] == MUR)
        *wind = 0;
    else if(posVent->y != pos->y && carte[posTab->x][posVent->y / TAILLE_BLOC] == MUR)
        *wind = 0;
    else
    {
        if(*wind == 1) //Deplacement quand sort deja lancé
        {
            switch (*direction_vent)
            {
                case 1 : //HAUT
                    posVent->y = posVent->y - DEPLACEMENT;
                    break;
                case 2 : //BAS
                    posVent->y = posVent->y + DEPLACEMENT;
                    break;
                case 3 : //GAUCHE
                    posVent->x = posVent->x - DEPLACEMENT;
                    break;
                case 4 : //DROIT
                    posVent->x = posVent->x + DEPLACEMENT;
                    break;
            }
        }
        else
            *wind = 1;
    }
}

void zoneVent(int tabSque[NBSQUE][CARASQUE],  SDL_Rect posVent, int nb_sque)
{
    int j = 0;

    //Vérifie collision du vent avec les squelettes:
    for (j = 0 ; j < nb_sque ; j ++)
    {
                if(posVent.x == tabSque[j][POSX])
                {
                    if(posVent.y == tabSque[j][POSY] || posVent.y - DEPLACEMENT == tabSque[j][POSY] || posVent.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posVent.y + DEPLACEMENT == tabSque[j][POSY] || posVent.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                        tabSque[j][VENT] = 1;
                }
                else if(posVent.x - DEPLACEMENT == tabSque[j][POSX])
                {
                    if(posVent.y == tabSque[j][POSY] || posVent.y - DEPLACEMENT == tabSque[j][POSY] || posVent.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posVent.y + DEPLACEMENT == tabSque[j][POSY] || posVent.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                        tabSque[j][VENT] = 1;
                }
                else if(posVent.x - 2 * DEPLACEMENT == tabSque[j][POSX])
                {
                    if(posVent.y == tabSque[j][POSY] || posVent.y - DEPLACEMENT == tabSque[j][POSY] || posVent.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posVent.y + DEPLACEMENT == tabSque[j][POSY] || posVent.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                        tabSque[j][VENT] = 1;
                }
                else if(posVent.x + DEPLACEMENT == tabSque[j][POSX])
                {
                    if(posVent.y == tabSque[j][POSY] || posVent.y - DEPLACEMENT == tabSque[j][POSY] || posVent.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posVent.y + DEPLACEMENT == tabSque[j][POSY] || posVent.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                        tabSque[j][VENT] = 1;
                }
                else if(posVent.x + 2 * DEPLACEMENT == tabSque[j][POSX])
                {
                    if(posVent.y == tabSque[j][POSY] || posVent.y - DEPLACEMENT == tabSque[j][POSY] || posVent.y - 2 * DEPLACEMENT == tabSque[j][POSY] || posVent.y + DEPLACEMENT == tabSque[j][POSY] || posVent.y + 2 * DEPLACEMENT == tabSque[j][POSY])
                        tabSque[j][VENT] = 1;
                }
    }
}

SDL_Rect deplacementSort(int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR],int tabTerre[3][3], int direction_vent, int *objectifsRestants, int i)
{
    int depv = 0, deph = 0;
    SDL_Rect positionTabSquelette, positionsquelette;

    positionTabSquelette.x = tabSquelette[i][TABX];
    positionTabSquelette.y = tabSquelette[i][TABY];
    positionsquelette.x = tabSquelette[i][POSX];
    positionsquelette.y = tabSquelette[i][POSY];
    depv = tabSquelette[i][DEPV];
    deph = tabSquelette[i][DEPH];
    deplacerJoueur(collision, &positionsquelette, &positionTabSquelette, direction_vent - 1, &depv, &deph, objectifsRestants, &direction_vent, tabSquelette, tabTerre, nb_sque, i);
    tabSquelette[i][TABX] = positionTabSquelette.x;
    tabSquelette[i][TABY] = positionTabSquelette.y;
    tabSquelette[i][POSX] = positionsquelette.x;
    tabSquelette[i][POSY] = positionsquelette.y;
    tabSquelette[i][FIXE] = 0;
    tabSquelette[i][HAUTE] = 0;
    tabSquelette[i][DEPV] = depv;
    tabSquelette[i][DEPH] = deph;
    return positionsquelette;
}

void pousserVent(SDL_Surface *ecran, int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR],int tabTerre[3][3], int direction, int *objectifsRestants, SDL_Surface *Squelette,SDL_Surface *SH,SDL_Surface *SB,SDL_Surface *SG,SDL_Surface *SD)
{
    int i = 0;
    SDL_Rect positionsquelette;

    //pousse les squelette toucher par le vent:
    for(i = 0; i < nb_sque; i++)
    {
        if(tabSquelette[i][VIE] > 0 && tabSquelette[i][VENT] == 1)
        {
            positionsquelette = deplacementSort(nb_sque, tabSquelette, collision, tabTerre, direction, objectifsRestants,i);
            if (tabSquelette[i][DIRSQUE] == 1)
                Squelette = SH;
            else if(tabSquelette[i][DIRSQUE] == 2)
                Squelette = SB;
            else if(tabSquelette[i][DIRSQUE] == 3)
                Squelette = SG;
            else if(tabSquelette[i][DIRSQUE] == 4)
                Squelette = SD;
            SDL_BlitSurface(Squelette, NULL, ecran, &positionsquelette);
        }
    }
}

void degatFeu(SDL_Surface *ecran, int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR],int tabTerre[3][3], int direction_boule,int *boule, int *explo, int *objectifsRestants, SDL_Surface *Squelette,SDL_Surface *SHR,SDL_Surface *SBR,SDL_Surface *SGR,SDL_Surface *SDR)
{
    int j = 0;
    SDL_Rect positionsquelette;

    //Deplacement du sque et degat quand toucher par une boule:
     if (*explo > -1 && *boule == 0)
    {
        positionsquelette = deplacementSort(nb_sque,tabSquelette,collision,tabTerre, direction_boule,objectifsRestants, *explo);
        tabSquelette[*explo][GLACER] = 0;
        if (direction_boule == 1)
            tabSquelette[*explo][DIRSQUE] = 2 ;
        else if (direction_boule == 2)
            tabSquelette[*explo][DIRSQUE] = 1;
        else if (direction_boule == 3)
            tabSquelette[*explo][DIRSQUE] = 4;
        else if (direction_boule == 4)
            tabSquelette[*explo][DIRSQUE] = 3;
        *boule = PORTEE + 1;
    }
    if(*explo > -1 )
    {
        positionsquelette.x = tabSquelette[*explo][POSX];
        positionsquelette.y = tabSquelette[*explo][POSY];
        if (tabSquelette[*explo][DIRSQUE] == 1)
            Squelette = SHR;
        else if(tabSquelette[*explo][DIRSQUE] == 2)
            Squelette = SBR;
        else if(tabSquelette[*explo][DIRSQUE] == 3)
            Squelette = SGR;
        else if(tabSquelette[*explo][DIRSQUE] == 4)
            Squelette = SDR;
        if(tabSquelette[*explo][VIE] > 0)
        {
            SDL_BlitSurface(Squelette, NULL, ecran, &positionsquelette);
        }
        else
        {
            for(j = 0; j <= 10; j++)
            {
                tabSquelette[*explo][j] = 0;
            }
        }
        *explo = -2;
    }
}

SDL_Surface *attaqueSque(int nb_sque, int *dejatoucher, int tabTerre[3][3], int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, SDL_Rect *posTab, int *a, int *b, int *objectifsRestants, int *direction_hero, SDL_Surface *hero, SDL_Surface *HR, SDL_Surface *BR, SDL_Surface *GR, SDL_Surface *DR)
{
    int toucher = 0, i = 0;
    printf("dejatoucher: %d\n", *dejatoucher);

    //Deplacement du joueur quand toucher par un sque:
    if (*dejatoucher == 0) // Evite d'être toucher plusieur fois
    {
        for (i = 0; i < nb_sque ; i++)
        {
            toucher = tabSquelette[i][TOUCHER];
            if(toucher == 1 && *dejatoucher == 0)
            {
                *dejatoucher = 1;
                if(tabSquelette[i][DIRSQUE] == 4 || tabSquelette[i][DIRSQUE] == 8) // attaque de la droite
                {
                    printf("ok");
                    deplacerJoueur(collision, pos, posTab, DROITE, a, b, objectifsRestants, direction_hero, tabSquelette, tabTerre, nb_sque, i);
                    hero = GR;
                    *direction_hero = 3;
                }
                else if(tabSquelette[i][DIRSQUE] == 3 || tabSquelette[i][DIRSQUE] == 7) //attaque de la gauche
                {
                    deplacerJoueur(collision, pos, posTab, GAUCHE, a, b, objectifsRestants, direction_hero, tabSquelette, tabTerre, nb_sque, i);
                    hero = DR;
                    *direction_hero = 4;
                }
                else if(tabSquelette[i][DIRSQUE] == 2 || tabSquelette[i][DIRSQUE] == 6) //attaque d'en bas
                {
                    deplacerJoueur(collision, pos, posTab, BAS, a, b, objectifsRestants, direction_hero, tabSquelette, tabTerre, nb_sque, -1);
                    hero = HR;
                    *direction_hero = 1;
                }
                else if(tabSquelette[i][DIRSQUE] == 1 || tabSquelette[i][DIRSQUE] == 5 ) // attaque d'en haut
                {
                    deplacerJoueur(collision, pos, posTab, HAUT, a, b, objectifsRestants, direction_hero, tabSquelette, tabTerre, nb_sque, -1);
                    hero = BR;
                    *direction_hero = 2;
                }
            }
            toucher = 0;
        }
    }
    return hero;
}
