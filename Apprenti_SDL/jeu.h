#ifndef JEU_H
#define JEU_H
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "constantes.h"
#include "Fonction_simple.h"
#include "fichier.h"
#include "editeur.h"
#include "hud.h"
#include "interaction.h"


void jouer(SDL_Surface *ecran, int progression, int *menu);

#endif
