#include"menu.h"

void menu(SDL_Surface *ecran)
{
    int menu = 2;
    SDL_Event event;
    SDL_Surface *curseur = NULL, *fond = NULL, *controles = NULL, *histoire = NULL;
    SDL_Rect pos, positionFond;

    //position des différentes positions du curseur:
    positionFond.x = 0, positionFond.y = 0, pos.x = 450, pos.y = 285;
    int posJouer = 285, posContinuer = 340, posEditeur = 460, posControles= 395, posQuitter = 515;
    int progression = 0;

    while(menu == 2)
    {
        curseur =  IMG_Load("Images/curseur.png");
        fond =  IMG_Load("Images/menu.png");
        controles =  IMG_Load("Images/controles.png");
        histoire =  IMG_Load("Images/histoire.png");
        menu = 1;

        //deplacement dans le menu:
        while (menu == 1)
        {
        SDL_BlitSurface(fond, NULL, ecran, &positionFond);
        SDL_BlitSurface(curseur, NULL, ecran, &pos);
        SDL_WaitEvent(&event);
        switch(event.type)
                {
                    case SDL_QUIT:
                        SDL_FreeSurface(curseur);
                        SDL_FreeSurface(fond);
                        SDL_FreeSurface(histoire);
                        SDL_FreeSurface(fond);
                        menu = 0;
                        break;
                     case SDL_KEYDOWN:
                        switch(event.key.keysym.sym)
                        {
                            case SDLK_ESCAPE:
                                SDL_FreeSurface(curseur);
                                SDL_FreeSurface(fond);
                                SDL_FreeSurface(histoire);
                                SDL_FreeSurface(fond);
                                menu = 0;
                                break;
                            case SDLK_UP:
                                if(pos.y == posJouer)               {pos.y=posQuitter;}
                                else if(pos.y == posContinuer)      {pos.y=posJouer;}
                                else if(pos.y == posControles)        {pos.y=posContinuer;}
                                else if(pos.y == posEditeur)      {pos.y=posControles;}
                                else if(pos.y == posQuitter)        {pos.y=posEditeur;}
                                break;
                            case SDLK_DOWN:
                                if(pos.y == posJouer)               {pos.y=posContinuer;}
                                else if(pos.y == posContinuer)      {pos.y=posControles;}
                                else if(pos.y == posControles)        {pos.y=posEditeur;}
                                else if(pos.y == posEditeur)      {pos.y=posQuitter;}
                                else if(pos.y == posQuitter)        {pos.y=posJouer;}
                                break;
                            case SDLK_SPACE:
                                if(pos.y == posEditeur)
                                {
                                    SDL_FreeSurface(controles);
                                    SDL_FreeSurface(curseur);
                                    SDL_FreeSurface(histoire);
                                    SDL_FreeSurface(fond);
                                    editeur(ecran);
                                    menu = 2;
                                }
                                else if(pos.y == posJouer)
                                {
                                    //Affichage de l'histoire du jeu

                                    SDL_BlitSurface(histoire, NULL, ecran, &positionFond);
                                    SDL_Flip(ecran);
                                    while (SDL_WaitEvent(&event) && SDL_KEYDOWN!=event.type);
                                    switch(event.key.keysym.sym)
                                    {
                                        default:
                                            SDL_FreeSurface(controles);
                                            SDL_FreeSurface(curseur);
                                            SDL_FreeSurface(fond);
                                            SDL_FreeSurface(histoire);
                                            progression=0;
                                            menu=2;
                                            //Debut du jeu
                                            jouer(ecran, progression, &menu);
                                            break;
                                    }
                                }
                                else if(pos.y == posContinuer)
                                {
                                    SDL_FreeSurface(controles);
                                    SDL_FreeSurface(curseur);
                                    SDL_FreeSurface(histoire);
                                    SDL_FreeSurface(fond);
                                    progression=1;
                                    menu=2;
                                    jouer(ecran, progression, &menu);
                                }
                                else if(pos.y == posControles)
                                {
                                    SDL_BlitSurface(controles, NULL, ecran, &positionFond);
                                    SDL_Flip(ecran);
                                    while (SDL_WaitEvent(&event) && SDL_KEYDOWN!=event.type);
                                    switch(event.key.keysym.sym)
                                    {
                                        default:
                                            break;
                                    }
                                }
                                else if(pos.y == posQuitter)    {menu=0;}
                                break;
                            default:
                                break;
                        }
                        break;
                }
        SDL_Flip(ecran);
        }
    }
}
