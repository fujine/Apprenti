#ifndef INTERACTION_H
#define INTERACTION_H
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "constantes.h"
#include "Fonction_simple.h"

void interagir(int carte[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], SDL_Rect pos, SDL_Rect posTab, int direction, int *glace, int *terre, int *vent, int *vie, int niveau);
void bouleFeu(SDL_Rect *pos, SDL_Rect *pos_bouledefeu, int *boule, int *direction_hero, int *direction_boule);
int explosion(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect posboule, int tabSque[NBSQUE][CARASQUE], int directionboule, int nb_sque);
void glacier(int carte[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, int tabsque[NBSQUE][CARASQUE], SDL_Rect *posGlace, int direction_hero, int nb_sque, int *direction_glace);
void murTerre(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect pos, SDL_Rect posTab, int *blocTerre, int tabTerre[3][3], int direction_hero, int tabSque[NBSQUE][CARASQUE], int nb_sque);
int creationMur(SDL_Rect posSuiv, SDL_Rect pos, int tabSque[NBSQUE][CARASQUE], int direction_terre, int tabTerre[3][3], int nb_sque);
void lamedeVent(int carte[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, SDL_Rect *posTab, SDL_Rect *posVent, int *direction_vent, int *wind , int direction_hero);
void zoneVent(int tabSque[NBSQUE][CARASQUE],  SDL_Rect posVent, int nb_sque);
SDL_Rect deplacementSort(int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR],int tabTerre[3][3], int direction, int *objectifsRestants, int i);
void pousserVent(SDL_Surface *ecran, int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR],int tabTerre[3][3], int direction_vent, int *objectifsRestants, SDL_Surface *Squelette,SDL_Surface *SH,SDL_Surface *SB,SDL_Surface *SG,SDL_Surface *SD);
SDL_Surface *attaqueSque(int nb_sque, int *dejatoucher, int tabTerre[3][3], int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, SDL_Rect *posTab, int *a, int *b, int *objectifsRestants, int *direction_hero,SDL_Surface *hero, SDL_Surface *HR, SDL_Surface *BR, SDL_Surface *GR, SDL_Surface *DR);
void degatFeu(SDL_Surface *ecran, int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR],int tabTerre[3][3], int direction_boule,int *boule, int *explo, int *objectifsRestants, SDL_Surface *Squelette,SDL_Surface *SHR,SDL_Surface *SBR,SDL_Surface *SGR,SDL_Surface *SDR);

#endif
