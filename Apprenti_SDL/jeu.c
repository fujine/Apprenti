#include"jeu.h"

void jouer(SDL_Surface *ecran, int progression, int *menu)
{
/****************************************************************************************************************/
        // Definitions et initialisations
/****************************************************************************************************************/
        //Hero
        SDL_Surface *hero[4] = {NULL}, *heroactuel = NULL, *herohaut2 = NULL,*herobas2 = NULL, *herogauche2 = NULL, *herodroit2 = NULL;
        SDL_Surface *herogaucherouge = NULL, *herodroitrouge = NULL, *herohautrouge = NULL, *herobasrouge = NULL;
        SDL_Surface *herobassort = NULL, *herohautsort = NULL, *herogauchesort = NULL, *herodroitsort = NULL;
        //Squelette
        SDL_Surface *squelette[4] = {NULL}, *SqueletteActuel = NULL,*squelettebas2 = NULL, *squelettehaut2 = NULL, *squelettedroit2 = NULL, *squelettegauche2 = NULL;
        SDL_Surface *squelettebasrouge = NULL, *squelettehautrouge = NULL, *squelettedroitrouge = NULL, *squelettegaucherouge = NULL;
        SDL_Surface *squelettebasgeler = NULL, *squelettehautgeler = NULL, *squelettedroitgeler = NULL, *squelettegauchegeler = NULL;
        //Décors
        SDL_Surface *arbre = NULL, *gel = NULL, *soldur = NULL, *chemin = NULL, *herbe = NULL, *mur = NULL, *porte = NULL,*ombre = NULL, *bouledefeu = NULL, *orbe = NULL;
        SDL_Surface *eau = NULL, *maisonbg = NULL, *maisonhg = NULL, * maisonhd = NULL, *maisonbd = NULL, *trou = NULL, *vase = NULL, *fontainep = NULL, *fondnoir = NULL, *murbas = NULL, *autel = NULL;
        //Sort
        SDL_Surface *bouledefeu1 = NULL, *bouledefeu2 = NULL, *bouledefeu3 = NULL, *bouledefeu4 = NULL, *boule_explo = NULL;
        SDL_Surface *givre = NULL, *givreh1 = NULL, *givreh2 = NULL, *givreh3 = NULL, *givrev1 = NULL, *givrev2 = NULL, *givrev3 = NULL;
        SDL_Surface *pierre = NULL, *souffle1 = NULL, *souffle2 = NULL, *souffle3 = NULL, *souffle4 = NULL;
        //Texte
        SDL_Surface *Game_over = NULL, *fin = NULL, *Sauvegarde = NULL;

        SDL_Event event;

        SDL_Rect position, positionJoueur, positionActuelle, positionOmbre;
        SDL_Rect pos_bouledefeu, posGlace, posTerre, posVent, posFond, posOrbre;

        int continuer = 1, jouer = 1, objectifsRestants = 0, i = 0, j = 0, tempsActuel = 0, tempsPrecedent = 0, niveau = 0;
        int compteur = 0, compteur3 = 0, c_ombre = 0;
            //Variable deplacement hero
        int a = 0, b = 0, direction_hero = 0, vie = 10;
            // Variable pour déplacement du squelette
        int dejatoucher = 0, nb_sque = 0, sque_mort = 0;
            //Tableau
        int carte[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};
        int tabSquelette[NBSQUE][CARASQUE] = {{0}};
        int collision[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};
        int tabvase[NB_BLOCS_LARGEUR][NB_BLOCS_HAUTEUR] = {{0}};
        int tabTerre[3][3] = {{0}};
            //Variable pour Sauvegarde
        int infojoueur[2]={0}, infotabjoueurs[2] = {0}, save = 0;
            //Variable de sort
        int  boule = PORTEE + 1, direction_boule = 0, rotationboule = 0, explo = -2, direction_vent = 0, sortglace = 0, direction_glace = 0, blocTerre = 0, wind = 0;
        int feu=2, terre=0, glace=0, vent=0, latence_feu = 0, latence_glace = 0, latence_terre = 0, latence_vent = 0;
            //Initialisation de la position des fonds.
        posFond.x = 0;
        posFond.y = 0;
/**************************************************************************************************************************************/
        // Chargement des sprites (decors, personnage...)
/**************************************************************************************************************************************/
        Sauvegarde = IMG_Load("Images/Sauvegarde.png");
        orbe = IMG_Load("Images/orbe.png"); fin = IMG_Load("Images/fin.png");
        Game_over = IMG_Load("Images/GameOver.png");
        souffle1 = IMG_Load("Images/souffle1.png"); souffle2 = IMG_Load("Images/souffle2.png");
        souffle3 = IMG_Load("Images/souffle3.png"); souffle4 = IMG_Load("Images/souffle4.png");
        pierre = IMG_Load("Images/terre.png"); boule_explo = IMG_Load("Images/explosion.png"); gel = IMG_Load("Images/gel.png");
        givreh1 = IMG_Load("Images/givreh1.png"); givreh2 = IMG_Load("Images/givreh2.png"); givreh3 = IMG_Load("Images/givreh3.png");
        givrev1 = IMG_Load("Images/givrev1.png"); givrev2 = IMG_Load("Images/givrev2.png"); givrev3 = IMG_Load("Images/givrev3.png");
        ombre = IMG_Load("Images/ombre.png"); mur = IMG_Load("Images/mur.png"); herbe = IMG_Load("Images/herbe.png");
        soldur = IMG_Load ("Images/soldur.png"); chemin = IMG_Load ("Images/chemin.png"); arbre = IMG_Load ("Images/arbre.png");
        porte = IMG_Load("Images/porte.png"); murbas = IMG_Load("Images/murbas.png");
        hero[BAS] = IMG_Load("Images/herobas.png"); hero[GAUCHE] = IMG_Load("Images/herogauche.png");
        hero[HAUT] = IMG_Load("Images/herohaut.png"); hero[DROITE] = IMG_Load("Images/herodroite.png");
        squelette[BAS] = IMG_Load("Images/squelettebas.png"); squelette[GAUCHE] = IMG_Load("Images/squelettegauche.png");
        squelette[HAUT] = IMG_Load("Images/squelettehaut.png"); squelette[DROITE] = IMG_Load("Images/squelettedroite.png");
        maisonbg = IMG_Load("Images/maisonbg.png"); maisonhg = IMG_Load("Images/maisonhg.png");
        maisonbd = IMG_Load("Images/maisonbd.png"); maisonhd = IMG_Load("Images/maisonhd.png");
        fontainep = IMG_Load("Images/fontainep.png"); eau = IMG_Load("Images/eau.png"); fondnoir = IMG_Load("Images/fondnoir.png");
        trou = IMG_Load("Images/trou.png"); vase = IMG_Load("Images/vase.png"); autel = IMG_Load("Images/autel.png");
        bouledefeu1 = IMG_Load("Images/bouledefeu.png"); bouledefeu2 = IMG_Load("Images/bouledefeu1.png");
        bouledefeu3 = IMG_Load("Images/bouledefeu2.png"); bouledefeu4 = IMG_Load("Images/bouledefeu3.png");
        herohaut2 = IMG_Load("Images/herohaut2.png"); herobas2 = IMG_Load("Images/herobas2.png");
        herodroit2 = IMG_Load("Images/herodroite2.png"); herogauche2 = IMG_Load("Images/herogauche2.png");
        squelettebas2 = IMG_Load("Images/squelettebas2.png"); squelettehaut2 = IMG_Load("Images/squelettehaut2.png");
        squelettedroit2 = IMG_Load("Images/squelettedroite2.png"); squelettegauche2 = IMG_Load("Images/squelettegauche2.png");
        squelettebasrouge = IMG_Load("Images/squelettebasrouge.png"); squelettehautrouge = IMG_Load("Images/squelettehautrouge.png");
        squelettedroitrouge = IMG_Load("Images/squelettedroiterouge.png"); squelettegaucherouge = IMG_Load("Images/squelettegaucherouge.png");
        squelettebasgeler = IMG_Load("Images/squelettebasgeler.png"); squelettehautgeler = IMG_Load("Images/squelettehautgeler.png");
        squelettedroitgeler = IMG_Load("Images/squelettedroitegeler.png"); squelettegauchegeler = IMG_Load("Images/squelettegauchegeler.png");
        herogaucherouge = IMG_Load("Images/herogaucherouge.png"); herodroitrouge = IMG_Load("Images/herodroiterouge.png");
        herobasrouge = IMG_Load("Images/herobasrouge.png"); herohautrouge = IMG_Load("Images/herohautrouge.png");
        herohautsort = IMG_Load("Images/herohautsort.png"); herobassort = IMG_Load("Images/herobassort.png");
        herogauchesort = IMG_Load("Images/herogauchesort.png"); herodroitsort = IMG_Load("Images/herodroitesort.png");

/**************************************************************************************************************************************/
    //Chargement du Niveau:
/**************************************************************************************************************************************/
    while (jouer && niveau<10)
    {

        // Initialisation squelette
        Initialiser(tabSquelette, tabTerre, &nb_sque, &sque_mort, &a,&b,&niveau,&objectifsRestants,&continuer,&dejatoucher,&direction_hero,&save);

        //Chargement du niveau
        chargement(carte, collision, tabvase, tabSquelette, &niveau, &vie, &objectifsRestants,&a,&b,&feu,&glace,&terre,&vent, &nb_sque,&progression, infotabjoueurs, infojoueur, &positionActuelle, &positionJoueur);

        //Position heros depart:
        heroactuel = hero[BAS];
        //Position squelette depart
        SqueletteActuel = squelette[BAS];

        // Activation de la repetition des touches, pour bouger le personnage
        SDL_EnableKeyRepeat(100, 100);

/**************************************************************************************************************************************/
    //Début du jeu:
/**************************************************************************************************************************************/
        while (continuer)
        {
            // Effet du sort de vent quand utilisé:
            if(wind == 1)
            {
                lamedeVent(collision, &positionActuelle, &positionJoueur, &posVent, &direction_vent, &wind, direction_hero);
                zoneVent(tabSquelette, posVent, nb_sque);
                // Reinitialisation de l'état vent sur les sque quand sort non actif
                if(wind == 0)
                {
                    for(i = 0; i < nb_sque; i++)
                    {
                        tabSquelette[i][VENT] = 0;
                    }
                }
            }

            //Début de la gestion du temps:
            tempsActuel = SDL_GetTicks();
            if (tempsActuel - tempsPrecedent > 50)
            {
                //Deplacement et intéraction de la boule de feu:
                bouleFeu(&positionActuelle, &pos_bouledefeu, &boule, &direction_hero, &direction_boule);


                //Gestion dégâts squelettes
                heroactuel = attaqueSque(nb_sque,&dejatoucher,tabTerre,tabSquelette,collision,&positionActuelle,&positionJoueur,&a,&b,&objectifsRestants,&direction_hero,heroactuel,herohautrouge,herobasrouge,herogaucherouge,herodroitrouge);
            }

            if (tempsActuel - tempsPrecedent > 100)
            {
                c_ombre = 0; // compteur ombre pour la deplacer en même temps que le personnages

/**************************************************************************************************************************************/
    //Action du Joueur:
/**************************************************************************************************************************************/
                SDL_PollEvent(&event);
                switch(event.type)
                {
                    case SDL_QUIT:
                        continuer = 0;
                        jouer = 0;
                        *menu = 0;
                        break;
                    case SDL_KEYDOWN:
                        switch(event.key.keysym.sym)
                        {
                            case SDLK_n:
                                continuer = 0;
                                break;
                            case SDLK_s:
                                if(save == 0)
                                {
                                    sauvegarde_en_jeu(carte, tabSquelette, positionJoueur, positionActuelle,a,b , objectifsRestants, niveau, vie, feu, glace, terre, vent, nb_sque);
                                    save = 30;// compteur pour l'affiche de la sauvegarde
                                }
                                break;
                            case SDLK_ESCAPE:
                                continuer = 0;
                                jouer = 0;
                                *menu = 2;
                                break;
                            case SDLK_UP:
                                if (heroactuel == hero[HAUT])
                                    heroactuel = herohaut2;
                                else
                                    heroactuel = hero[HAUT];
                                deplacerJoueur(collision, &positionActuelle, &positionJoueur, HAUT, &a, &b, &objectifsRestants, &direction_hero, tabSquelette, tabTerre, nb_sque, -1);
                                direction_hero = 1;
                                break;
                            case SDLK_DOWN:
                                if(heroactuel == hero[BAS])
                                    heroactuel = herobas2;
                                else
                                    heroactuel = hero[BAS];
                                deplacerJoueur(collision, &positionActuelle, &positionJoueur, BAS, &a, &b, &objectifsRestants, &direction_hero, tabSquelette, tabTerre, nb_sque, -1);
                                direction_hero = 2;
                                break;
                            case SDLK_RIGHT:
                                if(heroactuel == hero[DROITE])
                                    heroactuel = herodroit2;
                                else
                                    heroactuel = hero[DROITE];
                                deplacerJoueur(collision, &positionActuelle, &positionJoueur, DROITE, &a , &b, &objectifsRestants, &direction_hero, tabSquelette, tabTerre, nb_sque, -1);
                                direction_hero = 4;
                                break;
                            case SDLK_LEFT:
                                if (heroactuel == hero[GAUCHE])
                                    heroactuel = herogauche2;
                                else
                                    heroactuel = hero[GAUCHE];
                                deplacerJoueur(collision, &positionActuelle, &positionJoueur, GAUCHE, &a, &b, &objectifsRestants, &direction_hero, tabSquelette, tabTerre, nb_sque, -1);
                                direction_hero = 3;
                                break;
                            case SDLK_AMPERSAND: case SDLK_1:  // sort boule de feu
                                if(feu!=0)
                                {
                                    if(feu==2 || feu==1)
                                    {
                                        feu=1;
                                        if(latence_feu == 0)
                                        {
                                            latence_feu = 5;
                                            boule = 1;
                                            bouleFeu(&positionActuelle, &pos_bouledefeu, &boule, &direction_hero, &direction_boule);
                                            if(direction_hero == 1)
                                                heroactuel = herohautsort;
                                            else if(direction_hero == 2)
                                                heroactuel = herobassort;
                                            else if(direction_hero == 3)
                                                heroactuel = herogauchesort;
                                            else if(direction_hero == 4)
                                                heroactuel = herodroitsort;
                                        }
                                    }
                                }
                                if(vent!=0){vent=2;};
                                if(glace!=0){glace=2;};
                                if(terre!=0){terre=2;};
                                break;
                            case SDLK_2: case SDLK_WORLD_73: // Sort de Glace
                                if(glace!=0)
                                {
                                    if(glace==2 || glace==1)
                                    {
                                        glace=1;
                                        if(latence_glace == 0)
                                            {
                                                glacier(carte, collision, &positionActuelle, tabSquelette, &posGlace, direction_hero, nb_sque, &direction_glace);
                                                latence_glace = 40;
                                                sortglace = 4;
                                                if(direction_hero == 1)
                                                    heroactuel = herohautsort;
                                                else if(direction_hero == 2)
                                                    heroactuel = herobassort;
                                                else if(direction_hero == 3)
                                                    heroactuel = herogauchesort;
                                                else if(direction_hero == 4)
                                                    heroactuel = herodroitsort;
                                            }
                                    }
                                }
                                if(vent!=0){vent=2;};
                                if(terre!=0){terre=2;};
                                if(feu!=0){feu=2;};
                                break;
                            case SDLK_3: case SDLK_QUOTEDBL: //Sort de Terre
                                if(terre!=0)
                                {
                                    if(terre==2 || terre==1)
                                    {
                                        terre=1;
                                        if(latence_terre == 0)
                                        {
                                            murTerre(collision, positionActuelle, positionJoueur, &blocTerre, tabTerre, direction_hero, tabSquelette, nb_sque);
                                            latence_terre = 15;
                                            if(direction_hero == 1)

                                                heroactuel = herohautsort;
                                            else if(direction_hero == 2)
                                                heroactuel = herobassort;
                                            else if(direction_hero == 3)
                                                heroactuel = herogauchesort;
                                            else if(direction_hero == 4)
                                                heroactuel = herodroitsort;
                                        }
                                    }
                                }
                                if(vent!=0){vent=2;};
                                if(glace!=0){glace=2;};
                                if(feu!=0){feu=2;};
                                break;
                            case SDLK_4: case SDLK_QUOTE: // Sort de Vent
                                if(vent!=0)
                                {
                                    if(vent==2 || vent==1)
                                    {
                                        vent=1;
                                        if(latence_vent == 0)
                                        {
                                            lamedeVent(collision, &positionActuelle, &positionJoueur, &posVent,&direction_vent, &wind, direction_hero);
                                            latence_vent = 25;
                                            if(direction_hero == 1)
                                                heroactuel = herohautsort;
                                            else if(direction_hero == 2)
                                                heroactuel = herobassort;
                                            else if(direction_hero == 3)
                                                heroactuel = herogauchesort;
                                            else if(direction_hero == 4)
                                                heroactuel = herodroitsort;
                                        }
                                    }
                                }
                                if(terre!=0){terre=2;};
                                if(glace!=0){glace=2;};
                                if(feu!=0){feu=2;};
                                //Air;
                                break;
                            case SDLK_SPACE:
                                interagir(carte, tabvase, positionActuelle, positionJoueur, direction_hero, &glace, &terre, &vent, &vie, niveau);
                                break;
                            default:
                                break;
                        }
                        break;
                }

/**************************************************************************************************************************************/
    //Calcul des etats des variables:
/**************************************************************************************************************************************/
            for(i = 0; i < 3; i++)
            {
                if(tabTerre[i][ETAT] > 0)
                {
                    tabTerre[i][ETAT] = tabTerre[i][ETAT] - 1;  //Décrementation du temps d'apparition de la pierre
                    if(tabTerre[i][ETAT] == 0)
                    {
                        tabTerre[i][X] = 0;
                        tabTerre[i][Y] = 0;
                    }
                }
            }

            //Décrementation des latences:
            if(latence_feu != 0)
                latence_feu--;
            if(latence_glace != 0)
                latence_glace--;
            if(latence_terre != 0)
                latence_terre--;
            if(latence_vent != 0)
                latence_vent--;

            // Effacement de l'ecran
            SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

            //Nombre de squelette restant:
            sque_mort = nb_sque;
            for (i = 0; i < nb_sque; i ++)
            {
                if(tabSquelette[i][VIE] == 0)
                    sque_mort = sque_mort - 1;
            }

/**************************************************************************************************************************************/
    //Affichage du décor:
/**************************************************************************************************************************************/
            for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
            {
                for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
                {
                    position.x = i * TAILLE_BLOC;
                    position.y = j * TAILLE_BLOC;
                    switch(carte[i][j])
                    {
                        case MUR:
                            SDL_BlitSurface(mur, NULL, ecran, &position);
                            break;
                        case HERBE:
                            SDL_BlitSurface(herbe, NULL, ecran, &position);
                            break;
                        case SOLDUR:
                            SDL_BlitSurface(soldur, NULL, ecran, &position);
                            break;
                        case CHEMIN:
                            SDL_BlitSurface(chemin, NULL, ecran, &position);
                            break;
                        case ARBRE:
                            SDL_BlitSurface(arbre, NULL, ecran, &position);
                            break;
                        case PORTE:
                            if(niveau < 9)
                            {
                                if(niveau == 1)
                                    SDL_BlitSurface(chemin, NULL, ecran, &position);
                                else
                                    SDL_BlitSurface(porte, NULL, ecran, &position);
                                if(sque_mort != 0)
                                    collision[i][j] = 5;
                                else
                                    collision[i][j] = 6;
                            }
                            else
                            {
                                if(sque_mort == 0)
                                {
                                    SDL_BlitSurface(soldur, NULL, ecran, &position);
                                    collision[i][j] = 0;
                                }
                                else
                                {
                                    SDL_BlitSurface(porte, NULL, ecran, &position);
                                    collision[i][j] = 5;
                                }

                            }
                            break;
                        case MURBAS:
                            SDL_BlitSurface(murbas, NULL, ecran, &position);
                            break;
                        case TROU:
                            if (niveau < 3 || niveau == 10)
                                SDL_BlitSurface(herbe, NULL, ecran, &position);
                            else
                                SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(trou, NULL, ecran, &position);
                            break;
                        case AUTEL:
                            SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(autel, NULL, ecran, &position);
                            if(niveau == 10)
                                collision[i][j] = 7;
                            break;
                        case VASE:
                            if (niveau < 3 || niveau == 10)
                                SDL_BlitSurface(herbe, NULL, ecran, &position);
                            else
                                SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(vase, NULL, ecran, &position);
                            break;
                        case EAU:
                            SDL_BlitSurface(eau, NULL, ecran, &position);
                            break;
                        case FONTAINEP:
                            if (niveau < 3 || niveau == 10)
                                SDL_BlitSurface(herbe, NULL, ecran, &position);
                            else
                                SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(fontainep, NULL, ecran, &position);
                            break;
                        case FONDNOIR:
                            SDL_BlitSurface(fondnoir, NULL, ecran, &position);
                            break;
                        case MAISONBD:
                            if (niveau < 3 || niveau == 10)
                                SDL_BlitSurface(herbe, NULL, ecran, &position);
                            else
                                SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(maisonbd, NULL, ecran, &position);
                            break;
                        case MAISONHD:
                            if (niveau < 3 || niveau == 10)
                                SDL_BlitSurface(herbe, NULL, ecran, &position);
                            else
                                SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(maisonhd, NULL, ecran, &position);
                            break;
                        case MAISONBG:
                            if (niveau < 3 || niveau == 10)
                                SDL_BlitSurface(herbe, NULL, ecran, &position);
                            else
                                SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(maisonbg, NULL, ecran, &position);
                            break;
                        case MAISONHG:
                            if (niveau < 3 || niveau == 10)
                                SDL_BlitSurface(herbe, NULL, ecran, &position);
                            else
                                SDL_BlitSurface(soldur, NULL, ecran, &position);
                            SDL_BlitSurface(maisonhg, NULL, ecran, &position);
                            break;
                        case GEL:
                            SDL_BlitSurface(gel, NULL, ecran, &position);
                            break;
                        case ORBE:
                            posOrbre = position;
                            SDL_BlitSurface(soldur, NULL, ecran, &position);
                            break;
                    } //F switch
                }// F For
            }//F for

            // Si on n'a trouv eaucun objectif sur la carte, on relance sort de la boucle pour changer de niveau
            if (!objectifsRestants)
                continuer = 0;

/**************************************************************************************************************************************/
    //Gestion des sorts:
/**************************************************************************************************************************************/
            // Apparition des Pierre:
            for(i = 0; i < 3; i++)
            {
                if(tabTerre[i][ETAT] > 0)
                {
                    posTerre.x = tabTerre[i][X];
                    posTerre.y = tabTerre[i][Y];
                    SDL_BlitSurface(pierre, NULL, ecran, &posTerre);
                }
            }

             // Gestion du mouvement et attaque des squelettes
            utilisationSque(ecran,&compteur, explo, nb_sque,tabSquelette,collision,tabTerre,&positionActuelle,&vie,&dejatoucher,SqueletteActuel,squelettehautgeler,squelettebasgeler,squelettegauchegeler,squelettedroitgeler,squelette[DROITE],squelettedroit2,squelette[HAUT],squelettehaut2,squelette[BAS],squelettebas2,squelette[GAUCHE],squelettegauche2);

            // Apparition du sort de Glace:
            if(sortglace != 0 && compteur == 1)
            {
                if(direction_glace == 1)
                {
                    if(sortglace == 4)
                        givre = givreh1;
                    else if(sortglace == 3)
                        givre = givreh2;
                    else if (sortglace == 2)
                        givre = givreh3;
                    else
                        givre = givreh1;
                }
                else
                {
                    if(sortglace == 4)
                        givre = givrev1;
                    else if(sortglace == 3)
                        givre = givrev2;
                    else if (sortglace == 2)
                        givre = givrev3;
                    else
                        givre = givrev1;
                }
                sortglace = sortglace - 1;
                SDL_BlitSurface(givre, NULL, ecran, &posGlace);
            }

            tempsPrecedent = tempsActuel;
            }
            else
            {
               SDL_Delay(100 - (tempsActuel - tempsPrecedent));
            }
/**************************************************************************************************************************************/
    //Fin de la boucle de temps
/**************************************************************************************************************************************/

            //gestion boule de feu:
            if(boule <= PORTEE && boule != 0)
            {
                explo = explosion(collision, pos_bouledefeu, tabSquelette, direction_boule, nb_sque);
                if(boule <= PORTEE && continuer)
                {
                    if(rotationboule == 0 || bouledefeu == boule_explo)
                    {
                        rotationboule = 0;
                        bouledefeu = bouledefeu1;
                        rotationboule++;
                    }
                    else if(rotationboule == 1)
                    {
                        bouledefeu = bouledefeu2;
                        rotationboule++;
                    }
                    else if(rotationboule == 2)
                    {
                        bouledefeu = bouledefeu3;
                        rotationboule++;
                    }
                    else if(bouledefeu == bouledefeu3)
                    {
                        bouledefeu = bouledefeu4;
                        rotationboule = 0;
                    }
                    if (explo != -2)
                    {
                        bouledefeu = boule_explo;
                        boule = 0;
                    }
                    }
                }
            // Gestion affichage du sort vent
            if(wind == 1)
            {
                switch(direction_vent)
                {
                        case 1:
                        SDL_BlitSurface(souffle1, NULL, ecran, &posVent);
                        break;
                    case 2:
                        SDL_BlitSurface(souffle2, NULL, ecran, &posVent);
                        break;
                    case 3:
                        SDL_BlitSurface(souffle3, NULL, ecran, &posVent);
                        break;
                    case 4:
                        SDL_BlitSurface(souffle4, NULL, ecran, &posVent);
                        break;
                }

                //Deplacement ennemis avec le sort de vent:
                pousserVent(ecran, nb_sque,tabSquelette,collision,tabTerre,direction_vent,&objectifsRestants,SqueletteActuel,squelette[HAUT],squelette[BAS],squelette[GAUCHE],squelette[DROITE]);

            }
            if(boule <= PORTEE)
            {
                SDL_BlitSurface(bouledefeu, NULL, ecran, &pos_bouledefeu);
                if(boule == 0)
                    boule = PORTEE + 1;
            }

            //Effet du sort de feu sur les squelettes:
            degatFeu(ecran,nb_sque,tabSquelette,collision,tabTerre,direction_boule,&boule,&explo,&objectifsRestants,SqueletteActuel,squelettehautrouge,squelettebasrouge,squelettegaucherouge,squelettedroitrouge);

            //Deplacement de l'ombre:
            if(niveau > 2 && niveau != 10)
            {
                if(c_ombre == 0)
                {
                    positionOmbre.x = positionActuelle.x - 1304;
                    positionOmbre.y = positionActuelle.y - 644;
                    SDL_BlitSurface(ombre, NULL, ecran, &positionOmbre);
                    c_ombre = 1;
                }
            }

            //Affichage du joueur:
            SDL_BlitSurface(heroactuel, NULL, ecran, &positionActuelle);
            if(heroactuel == herobasrouge)
                heroactuel = hero[BAS];
            if(heroactuel == herohautrouge)
                heroactuel = hero[HAUT];
            if(heroactuel == herogaucherouge)
                heroactuel = hero[GAUCHE];
            if(heroactuel == herodroitrouge)
                heroactuel = hero[DROITE];
            if (explo > -1 && compteur3 < 2)
                compteur3++;
            if(niveau == 1 || niveau == 10)
                SDL_BlitSurface(orbe, NULL, ecran, &posOrbre);
            hud(ecran,vie,feu,glace,terre,vent,latence_feu, latence_glace, latence_terre, latence_vent, niveau);
            if(save > 0)
            {
                SDL_BlitSurface(Sauvegarde, NULL, ecran, &posFond);
                save --;
            }

            SDL_Flip(ecran);
            if(vie <= 0)
            {
                perdu(ecran, &vie, Game_over,posFond,&niveau,&continuer);
            }
            if(explo == -3)
            {
                findepartie(ecran,&continuer,fin,posFond);
            }

            while(SDL_PollEvent(&event));
        }// F While continuer
    }// F boucle jeu

/**************************************************************************************************************************************/
    //Fin du jeu
/**************************************************************************************************************************************/

        // Desactivation de la repetition des touches (remise  0)
        SDL_EnableKeyRepeat(0, 0);
        // Liberation des surfaces chargees

        SDL_FreeSurface(Sauvegarde);
        SDL_FreeSurface(givreh1); SDL_FreeSurface(givreh2); SDL_FreeSurface(givreh3);
        SDL_FreeSurface(givrev1); SDL_FreeSurface(givrev2); SDL_FreeSurface(givrev3);
        SDL_FreeSurface(pierre); SDL_FreeSurface(mur); SDL_FreeSurface(herbe); SDL_FreeSurface(arbre);
        SDL_FreeSurface(chemin); SDL_FreeSurface(soldur); SDL_FreeSurface(porte); SDL_FreeSurface(boule_explo);
        SDL_FreeSurface(ombre); SDL_FreeSurface(murbas); SDL_FreeSurface(maisonbg);
        SDL_FreeSurface(maisonhg); SDL_FreeSurface(maisonbd); SDL_FreeSurface(maisonhd);
        SDL_FreeSurface(fontainep); SDL_FreeSurface(eau); SDL_FreeSurface(fondnoir); SDL_FreeSurface(trou);
        SDL_FreeSurface(vase); SDL_FreeSurface(autel);
        SDL_FreeSurface(bouledefeu1); SDL_FreeSurface(bouledefeu2); SDL_FreeSurface(bouledefeu3); SDL_FreeSurface(bouledefeu4);
        SDL_FreeSurface(herohaut2); SDL_FreeSurface(herobas2); SDL_FreeSurface(herodroit2); SDL_FreeSurface(herogauche2);
        SDL_FreeSurface(squelettebas2); SDL_FreeSurface(squelettehaut2); SDL_FreeSurface(squelettedroit2); SDL_FreeSurface(squelettegauche2);
        SDL_FreeSurface(herogaucherouge); SDL_FreeSurface(herodroitrouge); SDL_FreeSurface(herohautrouge); SDL_FreeSurface(herobasrouge);
        SDL_FreeSurface(herohautsort); SDL_FreeSurface(herobassort); SDL_FreeSurface(herogauchesort); SDL_FreeSurface(herodroitsort);
        for (i = 0 ; i < 4 ; i++)
        SDL_FreeSurface(hero[i]);
        for (i = 0 ; i < 4 ; i++)
        SDL_FreeSurface(squelette[i]);
}
