#ifndef DEF_CONSTANTES
#define DEF_CONSTANTES
    #define TAILLE_BLOC      33
    #define NB_BLOCS_LARGEUR 40
    #define NB_BLOCS_HAUTEUR 20
    #define LARGEUR_FENETRE  TAILLE_BLOC * NB_BLOCS_LARGEUR
    #define HAUTEUR_FENETRE  TAILLE_BLOC * NB_BLOCS_HAUTEUR
    #define DEPLACEMENT TAILLE_BLOC / 3
    #define PORTEE  14
    #define MENUMOUV 20
    #define NBSQUE 40
    #define CARASQUE 13


    enum {HAUT,BAS,GAUCHE,DROITE};
    enum {VIDE, HERBE, SOLDUR, CHEMIN, ARBRE, MUR, PORTE, HERO, SQUELETTE, MURBAS, TROU, VASE, MAISONHD, MAISONBD, MAISONHG, MAISONBG, EAU, FONTAINEP, FONDNOIR, AUTEL, GEL, ORBE};
    enum {TABX, TABY, POSX, POSY, FIXE, HAUTE, DEPV, DEPH, DIRSQUE, TOUCHER, VIE, GLACER, VENT};
    enum {X, Y, ETAT, COMPTEUR};
//          0     1       2      3       4     5     6     7        8        9      10    11     12        13        14        15      16     17          18       19
#endif // DEF_CONSTANTES
